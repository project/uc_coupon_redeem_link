INTRODUCTION
------------
The Ubercart Discount Coupon Redeem Link module allows coupons to
be automatically redeemed by users upon visiting the page at 
/redeem-coupon/<COUPON_CODE>
This allows for a quick and easy method of automatically applying
coupon codes for users, negating the need for them to remember
the coupon code. Users can simply click the link provided and
the code will automatically appear on checkout. Very useful for
advertising/emails, etc.
  * For a full description of the module, visit the project page:
    http://drupal.org/project/uc_coupon_redeem_link
  * To submit bug reports and feature suggestions, or to track changes:
    http://drupal.org/project/issues/uc_coupon_redeem_link


REQUIREMENTS
------------
  * Installation of Ubercart module 
    http://drupal.org/project/ubercart
  * Installation of Ubercart module 
    http://drupal.org/project/uc_coupon


INSTALLATION
------------
  * Install as usual, see 
    https://www.drupal.org/documentation/install/modules-themes/modules-7 
    for further information.


TROUBLESHOOTING
---------------
  * Only valid, active coupons can be automatically redeemed. If the link
  is not working for your coupon, confirm the coupon is active and can be 
  applied directly on the checkout page.

MAINTAINERS
-----------
Current maintainers:
  * Daniel Moberly (Daniel.Moberly) - http://drupal.org/user/1160788

This project has been sponsored by:
  * Antique Electronic Supply
    All the parts you need to modify, repair, or build guitars, guitar 
    amplifiers, antique radios  and more. Visit 
    http://www.tubesandmore.com/ for more information.
