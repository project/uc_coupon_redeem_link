<?php
/**
 * @file
 * Ubercart Coupon Redeem Link.
 */

/**
 * Implements hook_help().
 */
function uc_coupon_redeem_link_help($path, $arg) {
  switch ($path) {
    case 'admin/help#uc_coupon_redeem_link':
      $output = '';
      $output .= '<p>' . t('The Ubercart Discount Coupon Redeem Link module allows coupons to be automatically redeemed by users upon visiting the page at /redeem-coupon/COUPON_CODE. After creating a valid Ubercart coupon, this link will immediately become usable for that coupon. For example, a coupon code with code "SALE15" can be automatically redeemed at <a href="@redeem">/redeem-coupon/SALE15</a>.', array('@redeem' => '/redeem-coupon/SALE15')) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function uc_coupon_redeem_link_menu() {
  $items = array();
  $items['redeem-coupon/%'] = array(
    'title' => 'Redeem coupon',
    'page callback' => 'uc_coupon_redeem_link_redeem_coupon',
    'access arguments' => array('redeem coupons by link'),
    'page arguments' => array(1),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function uc_coupon_redeem_link_permission() {
  // Defining permissions for redeeming coupons via link.
  return array(
    'redeem coupons by link' => array(
      'title' => t('Redeem coupons by link'),
      'description' => t('Allow user to redeem coupons directly through a redeem-coupon/COUPON link.'),
    ),
  );
}

/**
 * Page Callback for auto redeem without query arguments.
 *
 * This handles pages in the form /redeem-coupon/COUPONCODE.
 */
function uc_coupon_redeem_link_redeem_coupon($coupon_input) {
  // Cleanse the string.
  $coupon_input = t('@coupon', array('@coupon' => $coupon_input));

  // Load the coupon info.
  $coupon = uc_coupon_find($coupon_input);

  // If this is a valid coupon code, we apply it.
  if (isset($coupon->valid_from) && isset($coupon->valid_until) && isset($coupon->status) && ($coupon->valid_from < REQUEST_TIME || $coupon->valid_from == 0) && ($coupon->valid_until > REQUEST_TIME || $coupon->valid_until == 0) && $coupon->status == '1') {
    // We store the coupon code in the SESSION so that we can show it in the checkout field when possible.
    $_SESSION['uc_coupon_redeem'] = $coupon->code;
    // We store the coupon code in the SESSION so that it will be redeemed when possible.
    uc_coupon_session_add($coupon->code, 'retain');
    drupal_set_message(t('Thank you! You have successfully redeemed coupon "@code!" It will appear in your cart upon checkout.', array('@code' => $coupon->code)), 'status');
  }
  else {
    drupal_set_message(t('I\'m sorry, there was an error redeeming your coupon "@code". If you think this is in error, you can still apply the coupon directly at checkout.!', array('@code' => $coupon_input)), 'error');
  }

  // Redirect the page.
  drupal_goto();
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows application of the coupon code automatically at checkout.
 */
function uc_coupon_redeem_link_form_uc_cart_checkout_form_alter(&$form, &$form_state, $form_id) {
  // Apply coupon if it exists.
  if (isset($_SESSION['uc_coupon_redeem']) && !isset($form['panes']['coupon']['coupons'])) {
    $form['panes']['coupon']['code']['#value'] = $_SESSION['uc_coupon_redeem'];
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows application of the coupon code automatically at the cart page.
 */
function uc_coupon_redeem_link_form_uc_coupon_form_alter(&$form, &$form_state, $form_id) {
  // Apply coupon if it exists.
  if (isset($_SESSION['uc_coupon_redeem']) && !isset($form['coupons'])) {
    $form['code']['#value'] = $_SESSION['uc_coupon_redeem'];
  }
}
